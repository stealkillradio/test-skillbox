FROM node
RUN mkdir -p /flatris
WORKDIR /flatris
#не копировать все, а только список пакетов для установки
COPY package.json /flatris
RUN yarn install

#копировать уде все приложение
COPY . /flatris	

RUN yarn test
RUN yarn build

CMD yarn start

EXPOSE 3000
